﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DubiStor.UserHelper
{
    public class CurrentPublicSession
    {
        //public static HttpContext _HttpContextAccessor;
        //public static CurrentPublicUser PublicUser {
        //    get {
        //        CurrentPublicUser PublicUser = (CurrentPublicUser)_HttpContextAccessor.Session["CurrentPublicUser"];
        //        return PublicUser;
        //    }
        //    set {
        //        _HttpContextAccessor.Session["CurrentPublicUser"]= value;
        //    }
        //}
        const string ClientId_KEY = "CurrentPublicUser";

        public static CurrentPublicUser PublicUser {
            get { return HttpContext.Current.Session[ClientId_KEY] != null ? (CurrentPublicUser)HttpContext.Current.Session[ClientId_KEY] : null; }
            set { HttpContext.Current.Session[ClientId_KEY] = value; }
        }

        public static int PubUserID {
            get {
                if (PublicUser != null)
                    return PublicUser.PubUserID;
                else
                    return -1;
            }
        }

        public static string FirstName {
            get {
                if (PublicUser != null)
                    return PublicUser.FirstName;
                else
                    return string.Empty;
            }
        }
        public static string LastName {
            get {
                if (PublicUser != null)
                    return PublicUser.LastName;
                else
                    return string.Empty;
            }
        }
        public static string UserName {
            get {
                if (PublicUser != null)
                    return PublicUser.UserName;
                else
                    return string.Empty;
            }
        }
        public static string Email {
            get {
                if (PublicUser != null)
                    return PublicUser.Email;
                else
                    return string.Empty;
            }
        }

    }
    [Serializable]
    public class CurrentPublicUser
    {
        public int PubUserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}