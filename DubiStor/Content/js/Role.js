﻿$(function () {
    $("#Role").change(function () {
        $(".alert").hide();
        $(".validation-summary-errors").hide();
        IsRoleExists();
    });

});

function IsRoleExists() {
    if ($("#Role").val().trim().length > 0) {
        var SiteBaseUrl = SiteUrl + "Role/ValidateDuplicateRole";
        var data = { RoleID: $("#RoleID").val(), Role: $("#Role").val() };
        $.post(SiteBaseUrl, data, function (result) {
            if (result.status == "0") {
                $("#duplicateRecord").show();
                $("#btnSave").prop("disabled", true);
            }
            else {
                $("#duplicateRecord").hide();
                $("#btnSave").prop("disabled", false);
            }
        }, "json");
    }
}

function Check_CheckBox_Count(deleteName) {
    var $b = $('input[name=chkDelete]');
    var total_selected = $b.filter(':checked').length;
    if (total_selected > 0) {
        $.confirm({
            title: 'Alert!',
            content: 'Are you sure? Do you want to delete record(s)?',
            type: 'red',
            icon: 'fa fa-warning',
            buttons: {
                confirm: function () {
                    checked = [];
                    $("input[name=chkDelete]:checkbox").each(function () {
                        if (this.checked) {
                            checked.push(this.value);
                            var SiteBaseUrl = SiteUrl + "Role/Delete";
                            $.post(SiteBaseUrl, { roleID: checked }, function (result) {
                                if (result) {
                                    $('#BindRole').DataTable().ajax.reload();
                                }
                                else {
                                    alert("Something Went Wrong!");
                                }
                            });
                        }
                    });
                },
                cancel: function () {
                    return;
                }
            }
        });
    }
    else {
        alert("Please select at least one checkbox to delete.");
        return false;
    }
}