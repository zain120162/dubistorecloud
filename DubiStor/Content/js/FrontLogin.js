﻿$(document).ready(function () {
    DisableCutCopyPaste();
});

function DisableCutCopyPaste() {
    $("#Password").bind("cut copy paste", function (e) {
        e.preventDefault();
    });
}
