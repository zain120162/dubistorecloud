﻿$(document).ready(function () {
    //$("#RoleID").select2();

    var view = $("input[name='chkView']");
    var add = $("input[name='chkAdd']");
    var edit = $("input[name='chkEdit']");
    var del = $("input[name='chkDelete']");
    var details = $("input[name='chkDetail']");
    var approval = $("input[name='chkApproval']");
    if (view.length == view.filter(":checked").length)
        $("#chkViewAll").prop('checked', true)
    if (add.length == add.filter(":checked").length)
        $("#chkAddAll").prop('checked', true);
    if (edit.length == edit.filter(":checked").length)
        $("#chkEditAll").prop('checked', true);
    if (del.length == del.filter(":checked").length)
        $("#chkDeleteAll").prop('checked', true);
    if (details.length == details.filter(":checked").length)
        $("#chkDetailAll").prop('checked', true);
    if (approval.length == approval.filter(":checked").length)
        $("#chkApprovalAll").prop('checked', true);
    setTimeout(function () {
        if ((view.length == view.filter(":checked").length) && (add.length == add.filter(":checked").length) && (edit.length == edit.filter(":checked").length) && (del.length == del.filter(":checked").length) && (details.length == details.filter(":checked").length) && (approval.length == approval.filter(":checked").length)) {
            $("INPUT[name='chkSelectAll']").prop('checked', true);
            $("INPUT[name='chkMain']").prop('checked', true);
            $("INPUT[name='chkSubMain']").prop('checked', true);
            $("INPUT[name='chkAll']").prop('checked', true);
            $("#chkViewAll").prop('checked', true);
            $("#chkAddAll").prop('checked', true);
            $("#chkEditAll").prop('checked', true);
            $("#chkDeleteAll").prop('checked', true);
            $("#chkDetailAll").prop('checked', true);
            $("#chkApprovalAll").prop('checked', true);
        }
    }, 100);
    
    $('#chkSelectAll').click(function () {
        if ($('#chkSelectAll').is(':checked')) {
            $("INPUT[name='chkViewAll']").prop('checked', true);
            $("INPUT[name='chkAddAll']").prop('checked', true);
            $("INPUT[name='chkEditAll']").prop('checked', true);
            $("INPUT[name='chkDeleteAll']").prop('checked', true);
            $("INPUT[name='chkDetailAll']").prop('checked', true);
            $("INPUT[name='chkApprovalAll']").prop('checked', true);

            $("INPUT[name='chkView']").prop('checked', true);
            ViewDetailRights();
            $("INPUT[name='chkView1']").prop('checked', true);
            ViewDetailRights();
            $("INPUT[name='chkAdd']").prop('checked', true);
            ViewDetailRights();
            $("INPUT[name='chkEdit']").prop('checked', true);
            ViewDetailRights();
            $("INPUT[name='chkDelete']").prop('checked', true);
            ViewDetailRights();
            $("INPUT[name='chkDetail']").prop('checked', true);
            ViewDetailRights();
            $("INPUT[name='chkApproval']").prop('checked', true);
            ViewDetailRights();
            $("INPUT[name='chkAll']").prop('checked', true);
            ViewDetailRights();
            $("INPUT[name='chkMain']").prop('checked', true);
            ViewDetailRights();
            $("INPUT[name='chkSubMain']").prop('checked', true);
            ViewDetailRights();
        }
        else {
            $("INPUT[name='chkViewAll']").prop('checked', false);
            $("INPUT[name='chkAddAll']").prop('checked', false);
            $("INPUT[name='chkEditAll']").prop('checked', false);
            $("INPUT[name='chkDeleteAll']").prop('checked', false);
            $("INPUT[name='chkDetailAll']").prop('checked', false);
            $("INPUT[name='chkApprovalAll']").prop('checked', false);

            $("INPUT[name='chkView1']").prop('checked', false);
            ViewDetailRights();
            $("INPUT[name='chkView']").prop('checked', false);
            ViewDetailRights();
            $("INPUT[name='chkAdd']").prop('checked', false);
            ViewDetailRights();
            $("INPUT[name='chkEdit']").prop('checked', false);
            ViewDetailRights();
            $("INPUT[name='chkDelete']").prop('checked', false);
            ViewDetailRights();
            $("INPUT[name='chkDetail']").prop('checked', false);
            ViewDetailRights();
            $("INPUT[name='chkApproval']").prop('checked', false);
            ViewDetailRights();
            $("INPUT[name='chkAll']").prop('checked', false);
            ViewDetailRights();
            $("INPUT[name='chkMain']").prop('checked', false);
            ViewDetailRights();
            $("INPUT[name='chkSubMain']").prop('checked', false);
            ViewDetailRights();
        }
    });

    $('#chkViewAll').click(function () {
        if (!$("INPUT[name='chkView']").prop('checked', $('#chkViewAll').is(':checked')));
        if (!$(this).is(":checked")) {
            $("INPUT[name='chkEditAll']").prop('checked', false);
            $("INPUT[name='chkEdit']").prop('checked', false);
            $("INPUT[name='chkDetailAll']").prop('checked', false);
            $("INPUT[name='chkDetail']").prop('checked', false);
            $("INPUT[name='chkAddAll']").prop('checked', false);
            $("INPUT[name='chkAdd']").prop('checked', false);
            $("INPUT[name='chkDeleteAll']").prop('checked', false);
            $("INPUT[name='chkDelete']").prop('checked', false);
        }
        MainHeaderCheck();
    });
    $('#chkAddAll').click(function () {
        $("INPUT[name='chkAdd']").prop('checked', $('#chkAddAll').is(':checked'));
        ViewDetailRights();
        MainHeaderCheck();
    });
    $('#chkEditAll').click(function () {
        $("INPUT[name='chkEdit']").prop('checked', $('#chkEditAll').is(':checked'));
        ViewDetailRights();
        MainHeaderCheck();
    });
    $('#chkDeleteAll').click(function () {
        $("INPUT[name='chkDelete']").prop('checked', $('#chkDeleteAll').is(':checked'));
        ViewDetailRights();
        MainHeaderCheck();
    });
    $('#chkDetailAll').click(function () {
        $("INPUT[name='chkDetail']").prop('checked', $('#chkDetailAll').is(':checked'));
        ViewDetailRights();
        MainHeaderCheck();
    });
    $('#chkApprovalAll').click(function () {
        $("INPUT[name='chkApproval']").prop('checked', $('#chkApprovalAll').is(':checked'));
        MainHeaderCheck();
    });
});

function getPrivileges(id) {
    document.forms[1].submit();
    $("#RoleID").val() == 0;
}
function MainHeaderCheck() {
    if ($("INPUT[name='chkViewAll']").is(':checked') && $("INPUT[name='chkAddAll']").is(':checked') && $("INPUT[name='chkEditAll']").is(':checked') && $("INPUT[name='chkDeleteAll']").is(':checked') && $("INPUT[name='chkDetailAll']").is(':checked') && $("INPUT[name='chkApprovalAll']").is(':checked')) {
        $("INPUT[name='chkSelectAll']").prop('checked', true);
        $("INPUT[name='chkMain']").prop('checked', true);
        $("INPUT[name='chkSubMain']").prop('checked', true);
        $("INPUT[name='chkAll']").prop('checked', true);
    }
    else {
        $("INPUT[name='chkSelectAll']").prop('checked', false);
        $("INPUT[name='chkMain']").prop('checked', false);
        $("INPUT[name='chkSubMain']").prop('checked', false);
        $("INPUT[name='chkAll']").prop('checked', false);
    }
}
function SelectViewDetail(strMenuItemId, obj) {
    if (obj.checked) {
        $("input[value=v" + strMenuItemId + "]").prop('checked', true);
        //$("input[value=de" + strMenuItemId + "]").prop('checked', true);
    }
    if (!$("input[value=v" + strMenuItemId + "]").is(':checked')) {
        $("input[value=v" + strMenuItemId + "]").prop('checked', false);
        $("input[value=e" + strMenuItemId + "]").prop('checked', false);
        $("input[value=d" + strMenuItemId + "]").prop('checked', false);
        $("input[value=de" + strMenuItemId + "]").prop('checked', false);
        $("input[value=a" + strMenuItemId + "]").prop('checked', false);
    }

    if (($("input[value=a" + strMenuItemId + "]").is(':checked')) &&
        ($("input[value=d" + strMenuItemId + "]").is(':checked')) &&
        ($("input[value=de" + strMenuItemId + "]").is(':checked')) &&
        ($("input[value=v" + strMenuItemId + "]").is(':checked')) &&
        ($("input[value=e" + strMenuItemId + "]").is(':checked')) &&
        ($("input[value=ap" + strMenuItemId + "]").is(':checked'))) {
        $("input[value=vA" + strMenuItemId + "]").prop('checked', true);
    }
}
function SelectAllDetail(strMenuItemId, obj) {
    var Id = obj.id;
    var parentID = Id.split("_")[1];
    var oldParentID = Id.split("_");
    var ViewParentID = Id.replace(oldParentID[0], "View");
    var NewparentID = ViewParentID.split("_" + strMenuItemId);

    if (obj.checked) {
        $("input[value=v" + strMenuItemId + "]").prop('checked', true);
        $("input[value=de" + strMenuItemId + "]").prop('checked', true);
        $("input[value=d" + strMenuItemId + "]").prop('checked', true);
        $("input[value=a" + strMenuItemId + "]").prop('checked', true);
        $("input[value=e" + strMenuItemId + "]").prop('checked', true);
        var TotalChecked = 0;
        var TotalCheckboxes = 0;
        var a = $('[id*="Select_' + parentID + '"]');
        if (a.length == a.filter(":checked").length) {
            $("#" + NewparentID).prop('checked', true);
            $("#" + parentID).prop('checked', true);
        }
    }
    else {
        $("input[value=v" + strMenuItemId + "]").prop('checked', false);
        $("input[value=de" + strMenuItemId + "]").prop('checked', false);
        $("input[value=d" + strMenuItemId + "]").prop('checked', false);
        $("input[value=a" + strMenuItemId + "]").prop('checked', false);
        $("input[value=e" + strMenuItemId + "]").prop('checked', false);

        $("#" + NewparentID).prop('checked', false);
        $("#" + parentID).prop('checked', false);
    }
}
function ViewDetailRights() {
    if ($('#chkAddAll').is(':checked') || $('#chkEditAll').is(':checked') || $('#chkDeleteAll').is(':checked') || $('#chkDetailAll').is(':checked') || $('#chkApprovalAll').is(':checked')) {
        $("INPUT[name='chkView']").prop('checked', true);
        //$("INPUT[name='chkDetail']").prop('checked', true);
        $('#chkViewAll').prop('checked', true);
        // $('#chkDetailAll').prop('checked', true);
    }
}
function UncheckParentCheckbox(parentID, mainParentID, obj) {
    var p = obj.id;
    var q = p.split("_");
    if (!obj.checked) {
        var p1 = p.replace(q[0], "Select");
        var p2 = p.replace(q[0], "View");
        var p3 = q[1];
        var ChkView = p2.split("_" + q[2]);
        $("#" + p1).prop('checked', false);
        $("#" + ChkView[0]).prop('checked', false);
        $("#" + mainParentID).prop('checked', false);

        $("#chkSelectAll").prop('checked', false);
        if (q[0] == "View")
            $("#chkViewAll").prop('checked', false);
        else if (q[0] == "Add")
            $("#chkAddAll").prop('checked', false);
        else if (q[0] == "Edit")
            $("#chkEditAll").prop('checked', false);
        else if (q[0] == "Delete")
            $("#chkDeleteAll").prop('checked', false);
        else if (q[0] == "Detail")
            $("#chkDetailAll").prop('checked', false);

        else if (q[0] == "Select") {
            $("#chkViewAll").prop('checked', false);
            $("#chkAddAll").prop('checked', false);
            $("#chkEditAll").prop('checked', false);
            $("#chkDeleteAll").prop('checked', false);
            $("#chkDetailAll").prop('checked', false);
        }
        var parent = $('#ParentID');
        var mainParent = $('#MainParentID');
        if (parent != null) {
            parent.checked = false;
        }
        if (mainParent != null) {
            mainParent.checked = false;
        }
    }
    else {

        var p1 = p.replace(q[0], "Select");
        var p2 = p.replace(q[0], "View");
        var p3 = q[1];
        var ChkView = p2.split("_" + q[2]);

        var view = $("input[name='chkView']");
        var add = $("input[name='chkAdd']");
        var edit = $("input[name='chkEdit']");
        var del = $("input[name='chkDelete']");
        var details = $("input[name='chkDetail']");

        if (q[0] == "View") {
            if (view.length == view.filter(":checked").length)
                $("#chkViewAll").prop('checked', true);
        }
        else if (q[0] == "Add") {
            if (add.length == add.filter(":checked").length)
                $("#chkAddAll").prop('checked', true);
        }
        else if (q[0] == "Edit") {
            if (edit.length == edit.filter(":checked").length)
                $("#chkEditAll").prop('checked', true);
        }
        else if (q[0] == "Delete") {
            if (del.length == del.filter(":checked").length)
                $("#chkDeleteAll").prop('checked', true);
        }
        else if (q[0] == "Detail") {
            if (details.length == details.filter(":checked").length)
                $("#chkDetailAll").prop('checked', true);
        }
        setTimeout(function () {
            if ((view.length == view.filter(":checked").length) && (add.length == add.filter(":checked").length) && (edit.length == edit.filter(":checked").length) && (del.length == del.filter(":checked").length) && (details.length == details.filter(":checked").length)) {
                $("INPUT[name='chkSelectAll']").prop('checked', true);
                $("INPUT[name='chkMain']").prop('checked', true);
                $("INPUT[name='chkSubMain']").prop('checked', true);
                $("INPUT[name='chkAll']").prop('checked', true);
                $("#chkViewAll").prop('checked', true);
                $("#chkAddAll").prop('checked', true);
                $("#chkEditAll").prop('checked', true);
                $("#chkDeleteAll").prop('checked', true);
                $("#chkDetailAll").prop('checked', true);
            }
        }, 100);
    }
}
function CheckAllCheckbox(parentID, mainParentID, obj) {
    if (!obj.checked) {
        var parent = $('#ParentID');
        var mainParent = $('#MainParentID');
        if (parent != null) {
            parent.checked = false;
        }
        if (mainParent != null) {
            mainParent.checked = false;
        }
    }
}
function CheckUncheckParentCheckBox(parentClassName) {
    alert('uncheckParentcheckbox ' + parentClassName);
    if (parentClassName != "") {
        $(".grid-tab tr").find("input:checkbox").each(function () {
            if ($(this).attr('class') == parentClassName) {
                $(this).prop('checked', true);
            }
        });
    }
}
function CheckUncheckChildCheckBoxNew(parentID, type, control) {
    if (parentID != "") {
        $(".grid-tab tr").find("input:checkbox").each(function () {
            var obj = $(this);
            if (obj[0].id != null) {
                var arr = obj[0].id.split('_');
                if (arr.length > 1) {
                    if (arr[1] == parentID && arr[0] == type)
                        $(this).prop('checked', control.checked);
                }
            }
        });
    }
}
function UncheckMainParentCheckbox(mainParentID, obj) {
    if (!obj.checked) {
        var mainParent = $('#mainParentID');
        if (mainParent != null) {
            $("input[class='chkViewPar" + mainParentID + "']").prop('checked', false);
        }
    }
}
function CheckUncheckChildCheckBox(parentClassName, parentID) {
    var parentStatus = false;
    var subMenuAll = "subClass" + parentID;
    var childView = "chkView" + parentID;
    var childAdd = "chkAdd" + parentID;
    var childEdit = "chkEdit" + parentID;
    var childDelete = "chkDelete" + parentID;
    var childDetail = "chkDetail" + parentID;
    var childApproval = "chkApproval" + parentID;
    var childchkAll = "chkAll" + parentID;
    if (parentClassName != "") {
        if ($("input[class=" + parentClassName + "]").is(':checked')) {
            $("input[class=" + subMenuAll + "]").prop('checked', true);
            $("input[class=" + childView + "]").prop('checked', true);
            $("input[class=" + childAdd + "]").prop('checked', true);
            $("input[class=" + childEdit + "]").prop('checked', true);
            $("input[class=" + childDelete + "]").prop('checked', true);
            $("input[class=" + childDetail + "]").prop('checked', true);
            $("input[class=" + childApproval + "]").prop('checked', true);
            $("input[class=" + childchkAll + "]").prop('checked', true);

            setTimeout(function () {
                var view = $("input[name='chkView']");
                var add = $("input[name='chkAdd']");
                var edit = $("input[name='chkEdit']");
                var del = $("input[name='chkDelete']");
                var details = $("input[name='chkDetail']");
                var approval = $("input[name='chkApproval']");
                if ((view.length == view.filter(":checked").length) && (add.length == add.filter(":checked").length) && (edit.length == edit.filter(":checked").length) && (del.length == del.filter(":checked").length) && (details.length == details.filter(":checked").length) && (approval.length == approval.filter(":checked").length)) {
                    $("INPUT[name='chkSelectAll']").prop('checked', true);
                    $("INPUT[name='chkMain']").prop('checked', true);
                    $("INPUT[name='chkSubMain']").prop('checked', true);
                    $("INPUT[name='chkAll']").prop('checked', true);
                    $("#chkViewAll").prop('checked', true);
                    $("#chkAddAll").prop('checked', true);
                    $("#chkEditAll").prop('checked', true);
                    $("#chkDeleteAll").prop('checked', true);
                    $("#chkDetailAll").prop('checked', true);
                    $("#chkApprovalAll").prop('checked', true);
                }
            }, 100);
        }
        else {
            $("input[class=" + subMenuAll + "]").prop('checked', false);
            $("input[class=" + childView + "]").prop('checked', false);
            $("input[class=" + childAdd + "]").prop('checked', false);
            $("input[class=" + childEdit + "]").prop('checked', false);
            $("input[class=" + childDelete + "]").prop('checked', false);
            $("input[class=" + childDetail + "]").prop('checked', false);
            $("input[class=" + childApproval + "]").prop('checked', false);
            $("input[class=" + childchkAll + "]").prop('checked', false);

            $("#chkViewAll").prop('checked', false);
            $("#chkAddAll").prop('checked', false);
            $("#chkEditAll").prop('checked', false);
            $("#chkDeleteAll").prop('checked', false);
            $("#chkDetailAll").prop('checked', false);
            $("#chkApprovalAll").prop('checked', false);
            $("#chkSelectAll").prop('checked', false);
        }
    }
}
function CheckSubMenu(id, control) {
    $(".grid-tab tr").find("input:checkbox").each(function () {
        var obj = $(this);
        if (obj[0].id != null) {
            var arr = obj[0].id.split('_');
            if (arr.length > 1) {
                if (arr[1] == id)
                    $(this).prop('checked', control.checked);
            }
        }
    });
}
function CheckUnCheckMain(id) {
    var a = $("input[type='checkbox']." + id);
    if (a.length == a.filter(":checked").length) {
        $("input[class='chkViewPar1']").prop('checked', true);
        var view = $("input[name='chkView']");
        var add = $("input[name='chkAdd']");
        var edit = $("input[name='chkEdit']");
        var del = $("input[name='chkDelete']");
        var details = $("input[name='chkDetail']");
        setTimeout(function () {
            if ((view.length == view.filter(":checked").length) && (add.length == add.filter(":checked").length) && (edit.length == edit.filter(":checked").length) && (del.length == del.filter(":checked").length) && (details.length == details.filter(":checked").length)) {
                $("INPUT[name='chkSelectAll']").prop('checked', true);
                $("INPUT[name='chkMain']").prop('checked', true);
                $("INPUT[name='chkSubMain']").prop('checked', true);
                $("INPUT[name='chkAll']").prop('checked', true);
                $("#chkViewAll").prop('checked', true);
                $("#chkAddAll").prop('checked', true);
                $("#chkEditAll").prop('checked', true);
                $("#chkDeleteAll").prop('checked', true);
                $("#chkDetailAll").prop('checked', true);
            }
        }, 100);
    }
    else {

        $("input[class='chkViewPar1']").prop('checked', false);
        $("#chkViewAll").prop('checked', false);
        $("#chkAddAll").prop('checked', false);
        $("#chkEditAll").prop('checked', false);
        $("#chkDeleteAll").prop('checked', false);
        $("#chkDetailAll").prop('checked', false);
        $("#chkApprovalAll").prop('checked', false);
        $("#chkSelectAll").prop('checked', false);
    }
}
