﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Business;
using Business.IBusiness;
using DomainModel;
using DubiStor.infrastructure;
using DubiStor.Models;

namespace DubiStor.Areas.User.Controllers
{
    public class ProfileController : Controller
    {
        // GET: User/Profile
        IPublicUserBusiness publicuserbus;
        PublicUserDomainModel publicuserdm;
        IPreferencesBusiness preferencebus;
        PreferencesDomainModel preferencedm;
        public ProfileController() {
            publicuserbus = new PublicUserBusiness();
            publicuserdm = new PublicUserDomainModel();
            preferencebus = new PreferencesBusiness();
            preferencedm = new PreferencesDomainModel();

        }
        public ActionResult Index()
        {
            return View();
        }
        #region Edit Detail


        #region Change Password
       

        [HttpPost]
        public ActionResult ChangePassword(PublicUserViewModel publicuservm)
        {

         
            if (publicuservm.Password != publicuservm.ConfirmPassword) {
                return View(publicuservm);
            }
            var userDetail = publicuserbus.Get(publicuservm.PubUserID);
            if (userDetail != null) {
                //  string currentPassword = GlobalCode.Encryption.Decrypt(userDetail.Password);
                //objChangePasswordModel.NewPassword = GlobalCode.Encryption.Encrypt(objChangePasswordModel.NewPassword);
                PublicUserDomainModel publicuserdmobj = new PublicUserDomainModel();
                AutoMapper.Mapper.Map(publicuservm, publicuserdmobj);
                publicuserbus.EditPassword(publicuserdmobj);
                //HttpContext.Session.Clear();
                //HttpContext.Session.Remove("CurrentPublicUser");
                TempData["Msg"] = "changed";
                return RedirectToAction("Index", "Dashboard");
            }
            TempData["Msg"] = "Failed";
            return RedirectToAction("Index", "Dashboard");
        }
        #endregion



        [HttpGet]
        public ActionResult Edit(string id)
        {
            try {
                if (string.IsNullOrEmpty(id)) {
                    return RedirectToAction("Edit", "Profile");
                }
                else {
                    var UserID = int.Parse(id);
                    publicuserdm=publicuserbus.Get(UserID);
                    PublicUserViewModel publicuservm = new PublicUserViewModel();
                    AutoMapper.Mapper.Map(publicuserdm, publicuservm);
                    if (publicuserdm == null) {
                        TempData["Msg"] = "drop";
                        return RedirectToAction("Edit", "Profile");
                    }
                    publicuservm.Preference= preferencebus.GetAll().Where(x => x.isActive == true).Select(x => new SelectListItem { Text = x.Name, Value = x.PreferenceID.ToString() }).ToList();
                    string[] param =publicuservm.PreferenceIDs.Split(';');
                    for (int i = 0; i < param.Length-1; i++) {
                        for (int j=0;j< publicuservm.Preference.Count;j++) {

                            if (publicuservm.Preference[j].Value == param[i]) {
                                publicuservm.Preference[j].Selected = true;
                            }
                        }
                    }
                    return View(publicuservm);
                }
            }
            catch {
                TempData["Msg"] = "error";
                return RedirectToAction("Edit", "Profile");
            }
        }

        [HttpPost]
        public ActionResult EditDetail(PublicUserViewModel publicuservm, string publicUserID, AttachmentViewModel attachmentModel)
        {
            var publicUserDetail = publicuserbus.Get(publicuservm.PubUserID);
            string filename = "";
            if (publicUserDetail == null) {
                return RedirectToAction("Edit", "Profile");
            }
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0) {
                var attachments = attachmentModel.Attachments.Where(x => x != null).ToList();
                if (attachments.Any(x => !GlobalCode.AllowedDocumentTypes.Contains(Path.GetExtension(x.FileName)))) {
                //    ModelState.AddModelError("", string.Format(Messages.InvalidFile, string.Join(",", GlobalCode.AllowedDocumentTypes)));
                    return View(publicuservm);
                }

                if (attachments.Any(x => ((x.ContentLength / 1024) / 1024) > GlobalCode.DocumentMaxUploadSize)) {
                   // ModelState.AddModelError("", string.Format(Messages.MaxFileSizeExceeded, GlobalCode.DocumentMaxUploadSize));
                    return View(publicuservm);
                }
                filename= uploadfile(attachments);
            }
            TempData["Msg"] = "updated";
            AutoMapper.Mapper.Map(publicuservm, publicuserdm);
            if (filename != string.Empty) {
                string[] param = filename.Split(';');
                publicuserdm.Directory = param[0];
                publicuserdm.ProfilePicture = param[1];
            }
            foreach(var item in publicuservm.Preference) {
                if (item.Selected) {
                    publicuserdm.PreferenceIDs = publicuserdm.PreferenceIDs + item.Value + ";";
                }
            }
            publicuserbus.EditProfile(publicuserdm);
            return RedirectToAction("Index", "Dashboard");
        }
        #endregion
        public string uploadfile(List<HttpPostedFileBase> attachmentModel) {
            if (attachmentModel != null && attachmentModel.Count() > 0) {
                return SaveEditedFiles("ProfileImages", attachmentModel);
            }
            else {
                return "";
            }
        }
        public string SaveEditedFiles( string folderName, List<HttpPostedFileBase> files)
        {
            string filename = "";
            if (files != null && files.Count() > 0) {
                folderName = "Content\\ApplicationFiles\\" + folderName + "\\";
                string webRootPath = HostingEnvironment.ApplicationPhysicalPath;
                string basePath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(basePath))
                    System.IO.Directory.CreateDirectory(basePath);

                foreach (HttpPostedFileBase attachment in files) {
                    string GUID = Guid.NewGuid().ToString();
                    string datetime = DateTime.Now.ToString();
                    datetime= datetime.Replace(":", "");
                    datetime = datetime.Replace("/", "");
                    datetime = datetime.Replace(" ", "");
                    string fileName = attachment.FileName;
                    string fileExtension = Path.GetExtension(fileName);
                    fileName = GUID+"-"+ datetime + fileExtension;
                    string destinationPath = Path.Combine(basePath, fileName);
                    attachment.SaveAs(destinationPath);

                    filename = basePath + ";" + fileName;
                }
            }
            return filename;
        }
    }
}