﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business;
using Business.IBusiness;
using DomainModel;
using DubiStor.Models;

namespace DubiStor.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        public IUserBusiness userbus;
        public UserDomainModel userdm;
        public LoginController() {
            userbus = new UserBusiness();
            userdm = new UserDomainModel();
        }
        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignIn(UserViewModel uservm) {
            userdm=userbus.Get(uservm.Email);
            if (userdm != null) {
                if (userdm.Password == uservm.Password && userdm.Email == uservm.Email) {

                    return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
                }
                else {
                    TempData["Msg"] = "Username or password is incorrect";
                    return View("Index", uservm);
                }
            }
            else {
                TempData["Msg"] = "User Not Exist";
                return View("Index", uservm);
            }
        }
    }
}