﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business;
using Business.IBusiness;
using DomainModel;
using DubiStor.Models;

namespace DubiStor.Areas.Admin.Controllers
{
    public class MembersController : Controller
    {
        // GET: Admin/Members
        public IPublicUserBusiness publicuserbus;
        public List<PublicUserDomainModel> publicuserlistdm;
        public Message msg;
        public MembersController() {
            publicuserbus = new PublicUserBusiness();
            publicuserlistdm = new List<PublicUserDomainModel>();
            msg = new Message();
        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAll()
        {

            publicuserlistdm = publicuserbus.GetAll();
            return Json(new { data = publicuserlistdm }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddUpdate(PublicUserViewModel publicuservm)
        {
            PublicUserDomainModel publicuserdm = new PublicUserDomainModel();
            AutoMapper.Mapper.Map(publicuservm, publicuserdm);
            if (publicuserdm.PubUserID > 0) {
                msg = publicuserbus.Edit(publicuserdm);
            }
            else {
                msg = publicuserbus.Add(publicuserdm);
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public string Delete(int id)
        {
            msg = publicuserbus.Delete(id);
            return msg.Response;
        }
    }
}