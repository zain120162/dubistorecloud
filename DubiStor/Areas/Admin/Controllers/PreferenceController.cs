﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business;
using Business.IBusiness;
using DomainModel;
using DubiStor.Models;

namespace DubiStor.Areas.Admin.Controllers
{
    public class PreferenceController : Controller
    {
        // GET: Admin/Preference
        public IPreferencesBusiness preferencebus;
        public List<PreferencesDomainModel> preferencedm;
        public Message msg;
        public PreferenceController() {
            preferencebus = new PreferencesBusiness();
            preferencedm = new List<PreferencesDomainModel>();
            msg = new Message();
        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAll() {

            preferencedm=preferencebus.GetAll();
            return Json(new { data = preferencedm }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddUpdate(PreferenceViewModel preferencevm)
        {
            PreferencesDomainModel preferencedm = new PreferencesDomainModel();
            AutoMapper.Mapper.Map(preferencevm, preferencedm);
            if (preferencedm.PreferenceID > 0) {
                msg = preferencebus.Edit(preferencedm);
            }
            else {
                msg = preferencebus.Add(preferencedm);
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public string Delete(int id)
        {
            msg = preferencebus.Delete(id);
            return msg.Response;
        }
    }
}