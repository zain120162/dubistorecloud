﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DubiStor.Models
{
    public class TemplateViewModel
    {
        public int TemplateID { get; set; }
        public string TemplateName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}