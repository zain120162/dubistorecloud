﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DubiStor.Models
{
    public class UserViewModel
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<bool> isAdminUser { get; set; }
        public Nullable<int> RoleID { get; set; }
    }
}