﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DubiStor.Models
{
    public class PublicUserViewModel
    {
        public int PubUserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
        public string Directory { get; set; }
        public string ProfilePicture { get; set; }
        public System.DateTime DateJoined { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<bool> IsEmailVerfied { get; set; }
        public Nullable<bool> IsSocialLogin { get; set; }
        public List<SelectListItem> Preference { get; set; }
        public string PreferenceIDs { get; set; }
    }
}