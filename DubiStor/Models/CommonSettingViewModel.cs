﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DubiStor.Models
{
    public class CommonSettingViewModel
    {
        public int SettingID { get; set; }
        public string SMTPServer { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Nullable<int> Port { get; set; }
        public string SiteURL { get; set; }
        public Nullable<bool> IsSSL { get; set; }
    }
}