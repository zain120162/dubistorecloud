﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DubiStor.Models
{
    public class PreferenceViewModel
    {
        public int PreferenceID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}