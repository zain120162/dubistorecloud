﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Business;
using Business.IBusiness;
using DomainModel;
using DubiStor.infrastructure;
using DubiStor.Models;
using DubiStor.UserHelper;

namespace DubiStor.Controllers
{
    public class UserLoginController : Controller
    {
        public IPublicUserBusiness _publicuerbus;
        public ITemplateBusiness _templatebus;
        public TemplateDomainModel templatedm;
        public PublicUserDomainModel publicuserdm;
        public ICommonSettingBusiness _commonsettingbus;
        public Message msg;
        public UserLoginController()
        {
            _publicuerbus = new PublicUserBusiness();
            _templatebus = new TemplateBusiness();
            _commonsettingbus = new CommonSettingBusiness();
            publicuserdm = new PublicUserDomainModel();
            templatedm = new TemplateDomainModel();
            msg = new Message();
        }
        // GET: UserLogin
        public ActionResult Index()
        {
            return View();
        }
        #region Login
        public ActionResult Login()
        {
            return View();
        }
        #endregion

        [HttpPost]
        public ActionResult Login(PublicUserViewModel publicuservm)
        {
            publicuserdm=_publicuerbus.Get(publicuservm.Email);
            if (publicuserdm != null) {
                if (publicuserdm.Email == publicuservm.Email && publicuserdm.Password == publicuservm.Password) {
                    TempData["msg"] = "welcome";
                    CurrentPublicUser User = new CurrentPublicUser();
                    User.PubUserID = publicuserdm.PubUserID;
                    User.FirstName = publicuserdm.FirstName;
                    User.LastName = publicuserdm.LastName;
                    User.Email = publicuserdm.Email;
                    //  CurrentPublicSession._HttpContextAccessor("CurrentPublicUser", User);
                 //   Session["CurrentPublicUser"] = User;
                    CurrentPublicSession.PublicUser = User;
                    return RedirectToAction("Index", "Dashboard", new { area = "User" });
                }
                else {
                    TempData["Msg"] = "Username or Password is not correct";
                    return View(publicuservm);

                }
            }
            else {
                TempData["Msg"] = "Username not exist";
                return View(publicuservm);
            }
        }

        #region Register
        public ActionResult Register()
        {
            return View();
        }
        #endregion

        public ActionResult Privacy()
        {
            // Create a random state
            string state = Guid.NewGuid().ToString();
            // Create the url
            string url = "https://www.facebook.com/dialog/oauth?client_id=" + "228721164991378" + "&state=" +
            state + "&response_type=code&redirect_uri=" + "https://localhost:44355" + "/home/callback";
            // Redirect the user
            return Redirect(url);
        }
        #region Email body Html In profile create        
        private string PopulateEmailBody(PublicUserDomainModel result, string emailBody)
        {
            var siteUrl = _commonsettingbus.Get("smtp.zoho.com").SiteURL;
            emailBody = emailBody.Replace("#FirstName#", Convert.ToString(result.FirstName));
            emailBody = emailBody.Replace("#LastName#", Convert.ToString(result.LastName));
            emailBody = emailBody.Replace("#click here#", "<a href=" + string.Format(siteUrl) + "UserLogin/Verified?EmailID=" + GlobalCode.UrlEncrypt(result.Email) + ">click here</a>");

            return emailBody;
        }
        #endregion
        [HttpPost]
        public ActionResult Register(PublicUserViewModel publicuservm)
        {
            if (publicuservm.Password == publicuservm.ConfirmPassword) {
                publicuserdm = _publicuerbus.Get(publicuservm.Email);
                if (publicuserdm == null) {

                    publicuservm.IsEmailVerfied = false;
                    publicuservm.IsSocialLogin = false;
                    publicuservm.ProfilePicture = "";
                    publicuservm.Status = true;
                    publicuservm.DateJoined = DateTime.Now;
                    PublicUserDomainModel publicuserdmobj = new PublicUserDomainModel();
                    AutoMapper.Mapper.Map(publicuservm, publicuserdmobj);
                    msg = _publicuerbus.Add(publicuserdmobj);
                    if (msg.Result == true) {
                        templatedm=_templatebus.Get("Account Verification");
                        string emailBody = templatedm.Body;
                        emailBody = PopulateEmailBody(publicuserdmobj, emailBody);
                        SendModuleEmail(publicuserdmobj.Email, templatedm.Subject, emailBody);
                        TempData["Msg"] = "Email has been Sent on your mail address Kindly Verify it.";
                        return RedirectToAction("Message", "Home");
                        //return View(publicuservm);
                    }
                    else {
                        TempData["Msg"] = "Error Performing this Operation";
                        return View(publicuservm);
                    }

                }
                else {
                    TempData["Msg"] = "This Username already Exist";
                    return View(publicuservm);
                }
            }
            else {
                TempData["Msg"] = "Password Mismatch";
                return View(publicuservm);
            }
        }

        #region Send Email Module
        public bool SendModuleEmail(string recipients, string subject, string emailBody, string CC = "")
        {
            SmtpClient _SmtpClient = new SmtpClient();
            MailMessage _msgCsutomer = new MailMessage();
            bool isSend = false;
            var Commonsetting = _commonsettingbus.Get("smtp.zoho.com");
            string strFrom = Convert.ToString(Commonsetting.Email);
            _msgCsutomer.From = new MailAddress(strFrom);
            var arrrecipients = recipients.Split(',').ToList();
            foreach (var recipient in arrrecipients) {
                _msgCsutomer.To.Add(recipient);
            }

            if (!string.IsNullOrEmpty(CC)) {
                var arrCC = CC.Split(',').ToList();
                foreach (var cc in arrCC) {
                    _msgCsutomer.CC.Add(cc);
                }
            }
            _msgCsutomer.Subject = subject;
            _msgCsutomer.IsBodyHtml = true;
            _msgCsutomer.Body = emailBody;
            _SmtpClient.Host = Convert.ToString(Commonsetting.SMTPServer);
            _SmtpClient.Port = Convert.ToInt32(Commonsetting.Port);
            _SmtpClient.Credentials = new NetworkCredential(Commonsetting.Email, Commonsetting.Password);
            _SmtpClient.EnableSsl = Convert.ToBoolean(Commonsetting.IsSSL);
            _SmtpClient.Send(_msgCsutomer);
            isSend = true;
            return isSend;
        }
        #endregion
        public ActionResult Google()
        {
            //     var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + googleplus_redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;

            //https://accounts.google.com/o/oauth2/auth/oauthchooseaccount?response_type=code&client_id=505669636385-0ad3rmofgmhielkbknqim5k1s8d29a4d.apps.googleusercontent.com&redirect_uri=https%3A%2F%2Fauth.web.dev%2F__%2Fauth%2Fhandler&state=AMbdmDmKMJkyVEdZq33YB-UFktf-6xWTOC6gOqEJsx_kovXdqZztvjUf54WlJYb9HrspLze5PFyOYCQeiIUQkQwCdXlQ9GVl9GlKme-XRGj9lLXK72cQNtf56A6-S1zKvlrAgXDfvAQNweJLwm7PY_U29RViM3eYd0NNJYfof66RHsBtq9bMChzcThpP0-PibTkxPNqojHM7E0DuVfahyZxgyK2Z-1yTieKrIrqwOr8jgOqEX8W2NQci-H_tSha0AvXme47MvWCNTMqcHTTQTxsbmOoCmq3X2mStblTEaeF4XebnXcxE&scope=openid%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email%20profile&context_uri=https%3A%2F%2Fweb.dev&flowName=GeneralOAuthFlow
            // Create a random state
            string state = Guid.NewGuid().ToString();
            string url = "https://accounts.google.com/o/oauth2/v2/auth?" +
            "scope=openid email profile&" +
                //"include_granted_scopes=true&" +
                "response_type=code&" +
                "state=" + state + "&" +
                "redirect_uri=https://localhost:44322" + "/UserLogin/callbackg&" +
                "client_id=505669636385-0ad3rmofgmhielkbknqim5k1s8d29a4d.apps.googleusercontent.com";
            return Redirect(url);
        }

        public ActionResult FaceBook()
        {
            string state = Guid.NewGuid().ToString();
            var url = "https://www.facebook.com/v8.0/dialog/oauth?" +
                "response_type=code&" +
                "display=popup&" +
                "state=" + state + "&" +
                "redirect_uri=https://localhost:44322" + "/UserLogin/callback&" +
                "client_id=1605867736260802";
            //var facebookurl = "https://www.facebook.com/v8.0/dialog/oauth?&client_id=1605867736260802&display=popup&domain=https://localhost:44322/UserLogin/callback&e2e=%7B%7D&redirect_uri=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df3f109e2989fe0c%26domain%3Dlocalhost%26origin%3Dhttps%253A%252F%252Flocalhost%253A44322%252Ff1d1ca646c0d6c8%26relation%3Dopener.parent%26frame%3Df285d937f7b8e8&scope=public_profile%2Cemail&sdk=joey&url=https://localhost:44322/UserLogin/callback&version=v8.0";
            return Redirect(url);
        }
        #region forget password
        public ActionResult ForgetPassword()
        {
            return View();
        }
        #endregion
        public ActionResult Verified(string EmailID)
        {
            var emailID = GlobalCode.UrlDecrypt(EmailID);
            publicuserdm=_publicuerbus.Get(emailID);
            publicuserdm.IsEmailVerfied = true;
            _publicuerbus.Edit(publicuserdm);
            var templates = _templatebus.Get("Registeration");
            string emailBody = templates.Body;
            emailBody = PopulateEmailBodyforSuccessfullReg(publicuserdm, emailBody);
            SendModuleEmail(publicuserdm.Email, templates.Subject, emailBody);
            return View();
        }
        #region Email body Html In Registeration       
        private string PopulateEmailBodyforSuccessfullReg(PublicUserDomainModel result, string emailBody)
        {
            var siteUrl = _commonsettingbus.Get("smtp.zoho.com").SiteURL;
            emailBody = emailBody.Replace("#Login#", Convert.ToString(result.Email));
            emailBody = emailBody.Replace("#Password#", Convert.ToString(result.Password));
            return emailBody;
        }
        #endregion


      
        [HttpPost]
        public ActionResult ResetPassword(PublicUserViewModel objUserModel)
        {
           
            var userobj = _publicuerbus.Get(objUserModel.Email);
            if (userobj != null && userobj.IsEmailVerfied==true) {
                var templates = _templatebus.Get("Password Reset");
                string emailBody = templates.Body;
                emailBody = PopulateEmailBodyPasswordReset(userobj, emailBody);
                SendModuleEmail(objUserModel.Email, templates.Subject, emailBody);
                TempData["Msg"] = "Email has been Sent on your mail address .";
                return RedirectToAction("Message", "Home");

            }
            else {
                TempData["Msg"] = "This Email does not exist.";
                return RedirectToAction("Message", "Home");
            }
        }
        #region Email body Html In Forgot Password        
        private string PopulateEmailBodyPasswordReset(PublicUserDomainModel result, string emailBody)
        {
            var siteUrl = _commonsettingbus.Get("smtp.zoho.com").SiteURL;
            emailBody = emailBody.Replace("#FirstName#", Convert.ToString(result.FirstName));
            emailBody = emailBody.Replace("#LastName#", Convert.ToString(result.LastName));
            emailBody = emailBody.Replace("#click here#", "<a href=" + string.Format(siteUrl) + "UserLogin/ResetPassword?EmailID=" + GlobalCode.UrlEncrypt(result.Email) + ">click here</a>");

            return emailBody;
        }
        #endregion

        public ActionResult ResetPassword(string EmailID)
        {
            var emailID = GlobalCode.UrlDecrypt(EmailID);
            TempData["email"] = emailID;
            return View();
        }
        public ActionResult ResetNewPassword(PublicUserViewModel publicuser)
        {
            var userobj = _publicuerbus.Get(publicuser.Email);
            if (userobj != null) {
                if (publicuser.Password == publicuser.ConfirmPassword) {
                    userobj.Password = publicuser.Password;
                    _publicuerbus.Edit(userobj);
                    TempData["Msg"] = "Password Reset Successfully";
                    return RedirectToAction("Message", "Home");
                }
                else {
                    TempData["Msg"] = "password and confirm password mismatch .";
                    return RedirectToAction("Message", "Home");
                }
            }
            else {
                TempData["Msg"] = "Invalid Request";
                return RedirectToAction("Message", "Home");
            }

        }
    }
}