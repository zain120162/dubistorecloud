﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel;
using DubiStor.Models;

namespace DubiStor.infrastructure
{
    public class AutomapperWebProfile : AutoMapper.Profile
    {
        public static void Run()
        {

            AutoMapper.Mapper.Initialize(a => {
                a.AddProfile<AutomapperWebProfile>();
            }
            );

        }
        public AutomapperWebProfile()
        {
            CreateMap<PublicUserViewModel, PublicUserDomainModel>();
            CreateMap<PublicUserDomainModel, PublicUserViewModel>();

            CreateMap<UserDomainModel, UserViewModel>();
            CreateMap<UserViewModel, UserDomainModel>();

            CreateMap<PreferencesDomainModel, PreferenceViewModel>();
            CreateMap<PreferenceViewModel, PreferencesDomainModel>();



        }
    }
}