﻿using System.Web;
using System.Web.Optimization;

namespace DubiStor
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // bundles.UseCdn = true;
            //  var Icons = "https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css";
            //var fontawesome = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css";
            bundles.Add(new ScriptBundle("~/Js").Include(
                          "~/Content/aroma/vendors/jquery/jquery-3.2.1.min.js",
                          "~/Content/aroma/vendors/bootstrap/bootstrap.bundle.min.js",
                           "~/Content/aroma/vendors/skrollr.min.js",
                            "~/Content/aroma/vendors/owl-carousel/owl.carousel.min.js",
                             "~/Content/aroma/vendors/nice-select/jquery.nice-select.min.js",
                              "~/Content/aroma/vendors/jquery.ajaxchimp.min.js",
                               "~/Content/aroma/vendors/mail-script.js",
                                "~/Content/aroma/js/main.js",
                                  "~/Content/Sweetalert Scripts/Scripts/sweetalert.js",
                                  "~/Content/datatables/jquery.dataTables.js",
                                  "~/Content/datatables/dataTables.bootstrap.js"
                          ));


            bundles.Add(new StyleBundle("~/Css").Include(
                      "~/Content/aroma/vendors/bootstrap/bootstrap.min.css",
                      "~/Content/aroma/vendors/fontawesome/css/all.min.css",
                      "~/Content/aroma/vendors/themify-icons/themify-icons.css",
                      "~/Content/vendors/nice-select/nice-select.css",
                      "~/Content/vendors/owl-carousel/owl.theme.default.min.css",
                      "~/Content/vendors/owl-carousel/owl.carousel.min.css",
                      "~/Content/aroma/css/style.css",
                      "~/Content/aroma/css/Custom.css",
                      "~/Content/Sweetalert Scripts/Scripts/sweetalert.css"
                          ));
            BundleTable.EnableOptimizations = true;
        }
    }
}
