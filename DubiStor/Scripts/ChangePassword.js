﻿$(function () {
    $('#NewPassword').keyup(
        function () {
            $('#result').html(passwordStrengthForResetPassword($('#NewPassword').val()));
            if ($('#result').html() == "&nbsp;&nbsp;Too short" || $('#result').html() == "&nbsp;&nbsp;Average") {
                document.getElementById("result").style.color = 'red';
            }
            else {
                document.getElementById("result").style.color = 'green';
            }
        }
    )
    $("#NewPassword").change(function () {
        if (document.getElementById('NewPassword').value.length > 0) {
            document.getElementById("spanConfirm").innerHTML = 'Not Confirmed';
            document.getElementById("spanConfirm").style.color = 'red';
        }
        else {
            document.getElementById("spanConfirm").innerHTML = '';
        }
        return false;
    });
});

function funCofirmPassword() {
    if (document.getElementById('NewPassword').value.length > 0 && document.getElementById('ConfirmPassword').value.length > 0 && document.getElementById('NewPassword').value != document.getElementById('ConfirmPassword').value) {
        document.getElementById("spanConfirm").innerHTML = 'Passwords do not match.';
        document.getElementById("spanConfirm").style.color = 'red';
    }
    else if (document.getElementById('NewPassword').value.length > 0 && document.getElementById('ConfirmPassword').value.length == 0) {
        document.getElementById("spanConfirm").innerHTML = 'Not Confirmed';
        document.getElementById("spanConfirm").style.color = 'red';
    }
    else {
        document.getElementById("spanConfirm").innerHTML = 'Confirmed';
        document.getElementById("spanConfirm").style.color = 'green';
    }
}