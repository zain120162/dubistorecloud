﻿using Business.IBusiness;
using DataRepository;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class PublicUserBusiness : IPublicUserBusiness
    {
        public DubiStoreDBEntities dubistoredb;
        public PublicUserBusiness()
        {
            dubistoredb = new DubiStoreDBEntities();
        }
        public Message Add(PublicUserDomainModel publicuserdm)
        {
            Message message = new Message();
            try
            {
                PublicUser publicuser = new PublicUser { Address = publicuserdm.Address, DateJoined = publicuserdm.DateJoined,Email= publicuserdm.Email,FirstName= publicuserdm.FirstName,LastName= publicuserdm.LastName,Gender= publicuserdm.Gender,IsEmailVerfied= publicuserdm.IsEmailVerfied,IsSocialLogin= publicuserdm.IsSocialLogin,Phone= publicuserdm.Phone,ProfilePicture= publicuserdm.ProfilePicture,Status= publicuserdm.Status,Password=publicuserdm.Password,Directory=publicuserdm.Directory,PreferenceIDs=publicuserdm.PreferenceIDs };
                dubistoredb.PublicUsers.Add(publicuser);
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Added Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }

        public Message Delete(int PubUserID)
        {
            Message message = new Message();
            try
            {
                PublicUser publicuser = dubistoredb.PublicUsers.Find(PubUserID);
                dubistoredb.PublicUsers.Attach(publicuser);
                dubistoredb.PublicUsers.Remove(publicuser);
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Deleted Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }

        public Message Edit(PublicUserDomainModel publicuserdm)
        {
            Message message = new Message();
            try
            {
                PublicUser publicuser = dubistoredb.PublicUsers.Find(publicuserdm.PubUserID);
                publicuser.FirstName = publicuserdm.FirstName;
                publicuser.LastName = publicuserdm.LastName;
                publicuser.Phone = publicuserdm.Phone;
                publicuser.Email = publicuserdm.Email;
                publicuser.Password = publicuserdm.Password;
                publicuser.Address = publicuserdm.Address;
                publicuser.Gender = publicuserdm.Gender;
                publicuser.Directory = publicuserdm.Directory;
                publicuser.IsEmailVerfied = publicuserdm.IsEmailVerfied;
                publicuser.IsSocialLogin = publicuserdm.IsSocialLogin;
                publicuser.ProfilePicture = publicuserdm.ProfilePicture;
                publicuser.Status = publicuserdm.Status;
                publicuser.PreferenceIDs = publicuserdm.PreferenceIDs;
                dubistoredb.PublicUsers.Attach(publicuser);
                dubistoredb.Entry(publicuser).State = EntityState.Modified;
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Edited Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }




        public Message EditProfile(PublicUserDomainModel publicuserdm)
        {
            Message message = new Message();
            try
            {
                PublicUser publicuser = dubistoredb.PublicUsers.Find(publicuserdm.PubUserID);
                publicuser.FirstName = publicuserdm.FirstName;
                publicuser.LastName = publicuserdm.LastName;
                publicuser.Phone = publicuserdm.Phone;
                publicuser.Address = publicuserdm.Address;
                publicuser.Gender = publicuserdm.Gender;
                publicuser.Directory = publicuserdm.Directory;
                publicuser.ProfilePicture = publicuserdm.ProfilePicture;
                publicuser.PreferenceIDs = publicuserdm.PreferenceIDs;
                dubistoredb.PublicUsers.Attach(publicuser);
                dubistoredb.Entry(publicuser).State = EntityState.Modified;
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Edited Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }


        public Message EditPassword(PublicUserDomainModel publicuserdm)
        {
            Message message = new Message();
            try
            {
                PublicUser publicuser = dubistoredb.PublicUsers.Find(publicuserdm.PubUserID);
                publicuser.Password = publicuserdm.Password;
                dubistoredb.PublicUsers.Attach(publicuser);
                dubistoredb.Entry(publicuser).State = EntityState.Modified;
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Edited Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }
        public PublicUserDomainModel Get(int PubUserID)
        {
            PublicUserDomainModel publicuserdm = dubistoredb.PublicUsers.Where(x => x.PubUserID == PubUserID).Select(x => new PublicUserDomainModel { FirstName = x.FirstName, LastName = x.LastName,Address=x.Address,DateJoined=x.DateJoined,Email=x.Email,Gender=x.Gender,IsEmailVerfied=x.IsEmailVerfied,IsSocialLogin=x.IsSocialLogin,Phone=x.Phone,ProfilePicture=x.ProfilePicture,PubUserID=x.PubUserID,Status=x.Status,Password=x.Password,Directory=x.Directory,PreferenceIDs=x.PreferenceIDs }).SingleOrDefault();
            return publicuserdm;
        }

        public PublicUserDomainModel Get(string Email)
        {
            PublicUserDomainModel publicuserdm = dubistoredb.PublicUsers.Where(x => x.Email == Email).Select(x => new PublicUserDomainModel { FirstName = x.FirstName, LastName = x.LastName, Address = x.Address, DateJoined = x.DateJoined, Email = x.Email, Gender = x.Gender, IsEmailVerfied = x.IsEmailVerfied, IsSocialLogin = x.IsSocialLogin, Phone = x.Phone, ProfilePicture = x.ProfilePicture, PubUserID = x.PubUserID, Status = x.Status, Password = x.Password,Directory=x.Directory,PreferenceIDs=x.PreferenceIDs }).SingleOrDefault();
            return publicuserdm;
        }
        public List<PublicUserDomainModel> GetAll()
        {
            List<PublicUserDomainModel> publicuserdmlist = dubistoredb.PublicUsers.Select(x => new PublicUserDomainModel { FirstName = x.FirstName, LastName = x.LastName, Address = x.Address, DateJoined = x.DateJoined, Email = x.Email, Gender = x.Gender, IsEmailVerfied = x.IsEmailVerfied, IsSocialLogin = x.IsSocialLogin, Phone = x.Phone, ProfilePicture = x.ProfilePicture, PubUserID = x.PubUserID, Status = x.Status,Password=x.Password ,Directory=x.Directory,PreferenceIDs=x.PreferenceIDs}).ToList();
            return publicuserdmlist;
        }
    }
}
