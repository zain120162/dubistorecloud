﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.IBusiness
{
    public interface IPublicUserBusiness
    {
         Message Add(PublicUserDomainModel publicuserdm);
         Message Edit(PublicUserDomainModel publicuserdm);

         Message EditPassword(PublicUserDomainModel publicuserdm);
         Message EditProfile(PublicUserDomainModel publicuserdm);
         Message Delete(int PubUserID);
         PublicUserDomainModel Get(int PubUserID);
         List<PublicUserDomainModel> GetAll();

        PublicUserDomainModel Get(string Email);

    }
}
