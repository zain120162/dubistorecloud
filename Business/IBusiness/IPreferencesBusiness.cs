﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.IBusiness
{
    public interface IPreferencesBusiness
    {
        Message Add(PreferencesDomainModel preferencedm);
        Message Edit(PreferencesDomainModel preferencedm);
        Message Delete(int PreferenceID);
        PreferencesDomainModel Get(int PreferenceID);
        List<PreferencesDomainModel> GetAll();
    }
}
