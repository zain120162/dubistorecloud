﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.IBusiness
{
    public interface IUserBusiness
    {
        Message Add(UserDomainModel userdm);
        Message Edit(UserDomainModel userdm);
        Message Delete(int UserID);
        UserDomainModel Get(int UserID);
        List<UserDomainModel> GetAll();

        UserDomainModel Get(string Email);
    }
}
