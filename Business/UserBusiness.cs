﻿using Business.IBusiness;
using DataRepository;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class UserBusiness : IUserBusiness
    {
        public DubiStoreDBEntities dubistoredb;
        public UserBusiness()
        {
            dubistoredb = new DubiStoreDBEntities();
        }
        public Message Add(UserDomainModel userdm)
        {

            Message message = new Message();
            try
            {
                User user = new User {  Email=userdm.Email,FirstName= userdm.FirstName,LastName= userdm.LastName,isActive= userdm.isActive,isAdminUser= userdm.isAdminUser,Password= userdm.Password,Phone= userdm.Phone,RoleID= userdm.RoleID };
                dubistoredb.Users.Add(user);
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Added Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }

        public Message Delete(int UserID)
        {
            Message message = new Message();
            try
            {
                User user = dubistoredb.Users.Find(UserID);
                dubistoredb.Users.Attach(user);
                dubistoredb.Users.Remove(user);
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Deleted Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }

        public Message Edit(UserDomainModel userdm)
        {
            Message message = new Message();
            try
            {
                User user = dubistoredb.Users.Find(userdm.UserID);
                user.FirstName = userdm.FirstName;
                user.LastName = userdm.LastName;
                user.Phone = userdm.Phone;
                user.Email = userdm.Email;
                user.Password = userdm.Password;
                user.isActive = userdm.isActive;
                user.isAdminUser = userdm.isAdminUser;
                user.RoleID = userdm.RoleID;
                dubistoredb.Users.Attach(user);
                dubistoredb.Entry(user).State = EntityState.Modified;
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Edited Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }

        public UserDomainModel Get(int UserID)
        {
            UserDomainModel userdm = dubistoredb.Users.Where(x => x.UserID == UserID).Select(x => new UserDomainModel { FirstName = x.FirstName, LastName = x.LastName, isActive=x.isActive,UserID=x.UserID,RoleID=x.RoleID,isAdminUser=x.isAdminUser, Email = x.Email, Phone = x.Phone, Password = x.Password }).SingleOrDefault();
            return userdm;
        }

        public UserDomainModel Get(string Email)
        {
            UserDomainModel userdm = dubistoredb.Users.Where(x => x.Email == Email).Select(x => new UserDomainModel { FirstName = x.FirstName, LastName = x.LastName, isActive = x.isActive, UserID = x.UserID, RoleID = x.RoleID, isAdminUser = x.isAdminUser, Email = x.Email, Phone = x.Phone, Password = x.Password }).SingleOrDefault();
            return userdm;
        }

        public List<UserDomainModel> GetAll()
        {
            List<UserDomainModel> userdmlist = dubistoredb.Users.Select(x => new UserDomainModel { FirstName = x.FirstName, LastName = x.LastName, isActive = x.isActive, UserID = x.UserID, RoleID = x.RoleID, isAdminUser = x.isAdminUser, Email = x.Email, Phone = x.Phone, Password = x.Password }).ToList();
            return userdmlist;
        }
    }
}
