﻿using Business.IBusiness;
using DataRepository;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class CommonSettingBusiness : ICommonSettingBusiness
    {
        public DubiStoreDBEntities dubistoredb;
        public CommonSettingBusiness()
        {
            dubistoredb = new DubiStoreDBEntities();
        }
        public CommonSettingDomainModel Get(string name)
        {
            CommonSettingDomainModel comsettingdm = dubistoredb.CommonSettings.Where(x => x.SMTPServer == name).Select(x => new CommonSettingDomainModel { Email = x.Email, IsSSL = x.IsSSL, Password = x.Password, Port = x.Port,SettingID=x.SettingID,SiteURL=x.SiteURL,SMTPServer=x.SMTPServer }).SingleOrDefault();
            return comsettingdm;
        }
    }
}
