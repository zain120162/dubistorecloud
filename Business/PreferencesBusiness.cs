﻿using Business.IBusiness;
using DataRepository;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class PreferencesBusiness : IPreferencesBusiness
    {
        public DubiStoreDBEntities dubistoredb;
        public PreferencesBusiness()
        {
            dubistoredb = new DubiStoreDBEntities();
        }
        public Message Add(PreferencesDomainModel preferencedm)
        {
            Message message = new Message();
            try
            {
                Preference preference = new Preference {  Name = preferencedm.Name, Description = preferencedm.Description, isActive = preferencedm.isActive};
                dubistoredb.Preferences.Add(preference);
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Added Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }

        public Message Delete(int PreferenceID)
        {
            Message message = new Message();
            try
            {
                Preference preference = dubistoredb.Preferences.Find(PreferenceID);
                dubistoredb.Preferences.Attach(preference);
                dubistoredb.Preferences.Remove(preference);
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Deleted Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }

        public Message Edit(PreferencesDomainModel preferencedm)
        {
            Message message = new Message();
            try
            {
                Preference preference = dubistoredb.Preferences.Find(preferencedm.PreferenceID);
                preference.Name = preferencedm.Name;
                preference.Description = preferencedm.Description;
                preference.isActive = preferencedm.isActive;
                dubistoredb.Preferences.Attach(preference);
                dubistoredb.Entry(preference).State = EntityState.Modified;
                int result = dubistoredb.SaveChanges();
                if (result > 0)
                {
                    message.Result = true;
                    message.Response = "Record Edited Successfully";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                else
                {
                    message.Result = false;
                    message.Response = "Operation Failed";
                    message.Optional1 = "";
                    message.Optional2 = "";
                }
                return message;
            }
            catch (Exception ex)
            {
                message.Result = false;
                message.Response = ex.Message;
                message.Optional1 = "";
                message.Optional2 = "";
                return message;
            }
        }

        public PreferencesDomainModel Get(int PreferenceID)
        {
            PreferencesDomainModel preferencedm = dubistoredb.Preferences.Where(x => x.PreferenceID == PreferenceID).Select(x => new PreferencesDomainModel { Description = x.Description, isActive = x.isActive.HasValue, Name = x.Name, PreferenceID = x.PreferenceID }).SingleOrDefault();
            return preferencedm;
        }

        public List<PreferencesDomainModel> GetAll()
        {
            List<PreferencesDomainModel> preferencedm = dubistoredb.Preferences.Select(x => new PreferencesDomainModel {  Description = x.Description, isActive = x.isActive.HasValue, Name = x.Name, PreferenceID = x.PreferenceID }).ToList();
            return preferencedm;
        }
    }
}
