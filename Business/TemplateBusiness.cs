﻿using Business.IBusiness;
using DataRepository;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class TemplateBusiness : ITemplateBusiness
    {
        public DubiStoreDBEntities dubistoredb;
        public TemplateBusiness()
        {
            dubistoredb = new DubiStoreDBEntities();
        }
        public TemplateDomainModel Get(string name)
        {
            TemplateDomainModel templatedm = dubistoredb.Templates.Where(x => x.TemplateName == name).Select(x => new TemplateDomainModel {TemplateID=x.TemplateID,TemplateName=x.TemplateName,Body=x.Body,Subject=x.Subject}).SingleOrDefault();
            return templatedm;
        }
    }
}
