﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel
{
    public class Message
    {
        public bool Result { get; set; }
        public string Response { get; set; }
        public string Optional1 { get; set; }
        public string Optional2 { get; set; }
    }
}
